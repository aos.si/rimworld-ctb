using System;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace TechBackground
{
    public static class Toil_Sit
    {

	public static Toil TryToSitAtStudyDesk()
	{
	    Toil toil = new Toil();
	    toil.initAction = delegate
	    {
		Pawn actor = toil.actor;
		actor.pather.StartPath(actor.CurJob.targetB, PathEndMode.OnCell);
	    };
	    toil.defaultCompleteMode = ToilCompleteMode.PatherArrival;

	    return toil;
	}

	public static bool ValidateChair(Thing desk, Pawn actor, Thing t, bool forced)
	{
	    if (t.def.building == null || !t.def.building.isSittable)
	    {
		return false;
	    }
	    if (t.IsForbidden(actor))
	    {
		return false;
	    }
	    if (!actor.CanReserve(t, 1, -1, null, forced))
	    {
		return false;
	    }
	    if (!t.IsSociallyProper(actor))
	    {
		return false;
	    }
	    if (t.IsBurning())
	    {
		return false;
	    }
	    if (t.HostileTo(actor))
	    {
		return false;
	    }
	    bool result = false;
	    for (int i = 0; i < 4; i++)
	    {
		IntVec3 c = t.Position + GenAdj.CardinalDirections[i];
		Building edifice = c.GetEdifice(t.Map);
		if (edifice == desk)
		{
		    result = true;
		    break;
		}
	    }
	    return result;
	}

	public static bool ValidateSpot(Thing desk, Pawn actor, IntVec3 spot, bool forced)
	{
	    return (spot.Standable(actor.Map) && actor.CanReserve(spot, 1, -1, null, forced));
	}

	public static bool ValidateTargetInfo(Thing desk, Pawn actor, LocalTargetInfo info, bool forced)
	{
	    if (info == null) return false;
	    if (info.HasThing)
	    {
		return ValidateChair(desk, actor, info.Thing, forced);
	    }
	    return ValidateSpot(desk, actor, info.Cell, forced);
	}


	public static Thing FindChair(Thing desk, Pawn actor, bool forced = false)
	{
	    Thing chair = null;

	    Predicate<Thing> baseChairValidator = delegate(Thing t)
	    {
		return ValidateChair(desk, actor, t, forced);
	    };

	    chair = GenClosest.ClosestThingReachable(desk.Position, actor.Map, ThingRequest.ForGroup(ThingRequestGroup.BuildingArtificial),
		    PathEndMode.OnCell, TraverseParms.For(actor, Danger.Deadly, TraverseMode.ByPawn, false), 25,
		    (Thing t) => baseChairValidator(t) && t.Position.GetDangerFor(actor, t.Map) == Danger.None,
		    null, 0, -1, false, RegionType.Set_Passable, false);

	    return chair;
	}

	public static bool FindSpot(Thing desk, Pawn actor, out IntVec3 spot, bool forced = false)
	{
	    foreach (IntVec3 current in GenAdj.CellsAdjacentCardinal(desk).InRandomOrder(null))
	    {
		if (ValidateSpot(desk, actor, current, forced))
		{
		    spot = current;
		    return true;
		}
	    }

	    spot = IntVec3.Invalid;
	    return false;
	}
    }

}

