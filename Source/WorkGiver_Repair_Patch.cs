using System;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace TechBackground
{
    [HarmonyPatch(typeof(WorkGiver_Repair), "HasJobOnThing")]
	public class Repair_HasJobOnThing
	{
	    [HarmonyPostfix]
		static void Postfix(ref bool __result, Pawn pawn, Thing t)
		{
			if (!__result) return;
			TraitDef def = TraitDef.Named("TechBackground");
			if (def == null)
			{
				Log.Message("Couldn't find Trait TechBackground");
				return;
			}
			int pawn_tech_level = 0;

			if (pawn.story.traits.HasTrait(def))
			{
				Trait tr = pawn.story.traits.GetTrait(def);
				pawn_tech_level = tr.Degree;
			}

			TechLevel techLevel = TechLevel.Animal;
			BuildableDef b = t.def;
			bool can = false;

			List<ResearchProjectDef> prereq_list = b.researchPrerequisites;


			if (prereq_list != null)
			{
				for (int i = 0; i < prereq_list.Count; i++)
				{
					if (prereq_list[i].techLevel > techLevel) techLevel = prereq_list[i].techLevel;
				}
			}

			can = TechLevelMatcher.Match(pawn_tech_level, techLevel);
			can = can && TechLevelMatcher.Hardcoded_Restrictions(b, pawn_tech_level);
			if (!can)
			{
				JobFailReason.Is("Colonist's tech background is too low.");
			}

			__result = can && __result;
		}

	}
}
