using System;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace TechBackground
{
    public class WorkGiver_Mentor : WorkGiver_Scanner
    {
	public override ThingRequest PotentialWorkThingRequest
	{
	    get
	    {
		return ThingRequest.ForGroup(ThingRequestGroup.BuildingArtificial);
	    }
	}

	public override bool HasJobOnThing(Pawn pawn, Thing t, bool forced = false)
	{
            TraitDef def = TraitDef.Named("TechBackground");
            Trait trait = pawn.story.traits.GetTrait(def);

            Building target = t as Building;
            if (target == null) return false;
            Comp_StudyDesk c = target.GetComp<Comp_StudyDesk>();
            if (c == null) return false;
            if (!c.Active) return false;

	    if (!forced && (c.mentor != null))
	    {
		JobFailReason.Is("There is another mentor at this desk.");
		return false;
	    }

	    if (c.students.Count == 0)
	    {
		JobFailReason.Is("No colonists are studying at this desk.");
		return false;
	    }

	    int min_degree = trait.Degree;
	    for (int i = 0; i < c.students.Count; i++)
	    {
		int sd = c.students[i].story.traits.GetTrait(def).Degree;
		float m = c.students[i].GetComp<Comp_TechBackground_Data>().mentoring;
		if ((sd < min_degree) && (m < JobDriver_Mentor.MentoringCap / 2))
		{
		    min_degree = sd;
		}
	    }

	    if (min_degree >= trait.Degree)
	    {
		JobFailReason.Is("Can not mentor any colonists at this desk.");
		return false;
	    }

	    LocalTargetInfo info = c.NextValidSpot(pawn, forced);

	    if (info == null)
	    {   
		JobFailReason.Is("No available spots around this desk.");
		return false;
	    }

	    return pawn.CanReserve(target, c.MaxPawns, 0, null, forced);
	}

	public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false)
	{   
	    Building target = t as Building;
	    if (target == null) return null;
	    Comp_StudyDesk c = target.GetComp<Comp_StudyDesk>();
	    if (c == null) return null;

	    LocalTargetInfo info = c.NextValidSpot(pawn, forced);
	    return new Job(DefDatabase<JobDef>.GetNamed("TechBackground_Job_Mentor"), t, info);
	}

    }
}


