using System;
using RimWorld;
using Verse;
using System.Collections.Generic;


namespace TechBackground
{
    public static class TechLevelMatcher
    {

	public static bool Match(int trait, TechLevel techLevel)
	{
	    switch(trait)
	    {
		case 0:
		    if (techLevel <= TechLevel.Medieval) return true;
		    break;
		case 1:
		    if (techLevel <= TechLevel.Industrial) return true;
		    break;
		case 2:
		    if (techLevel <= TechLevel.Spacer) return true;
		    break;
		case 3:
		    return true;
	    }

	    return false;
	}

	public static int TechLevelToTechBackground(TechLevel techLevel)
	{
	    if (techLevel <= TechLevel.Medieval) return 0;
	    if (techLevel <= TechLevel.Industrial) return 1;
	    if (techLevel <= TechLevel.Spacer) return 2;
	    return 3;
	}

	public static bool Hardcoded_Restrictions(BuildableDef def, int techLevel)
	{
	    if ((def.defName == "PsychicEmanator") && (techLevel < 3)) return false;
	    if ((def.defName == "VanometricPowerCell") && (techLevel < 3)) return false;
	    if ((def.defName == "InfiniteChemreactor") && (techLevel < 3)) return false;
	    if ((def.defName == "FlatscreenTelevision") && (techLevel < 1)) return false;
	    if ((def.defName == "MegascreenTelevision") && (techLevel < 1)) return false;
	    if ((def.defName == "Telescope") && (techLevel < 1)) return false;

	    return true;
	}

    }
}


