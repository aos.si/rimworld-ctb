using System;
using RimWorld;
using Verse;
using System.Collections.Generic;
using UnityEngine;

namespace TechBackground
{
	public class Comp_StudyDesk : ThingComp
	{
		private bool active = false;

		public List<Pawn> students = new List<Pawn>();
		public Pawn mentor = null;

		public LocalTargetInfo next_spot = null;


		public bool Active
		{
			get
			{
				return active;
			}
			set
			{
				if (value == active) return;
				active = value;
			}
		}

		public int MaxPawns
		{
			get
			{
				return (props as CompProperties_StudyDesk).MaxPawns;
			}
		}

		public LocalTargetInfo NextValidSpot(Pawn pawn, bool forced)
		{
			// First try to find non-forced spot
			if (Toil_Sit.ValidateTargetInfo(parent, pawn, next_spot, false)) return next_spot;
			LocalTargetInfo old_spot = next_spot;
			SelectValidSpot(pawn, false);

			if (!forced) return next_spot;
			if (next_spot != null) return next_spot;

			if (Toil_Sit.ValidateTargetInfo(parent, pawn, old_spot, forced)) next_spot = old_spot;
			else SelectValidSpot(pawn, forced);

			return next_spot;

			//Log.Message("Picked next valid spot [" + parent + "]: " + next_spot);
		}

		private void SelectValidSpot(Pawn pawn, bool forced)
		{
			IntVec3 spot;
			Thing chair = Toil_Sit.FindChair(parent, pawn, forced);
			if (chair == null)
			{   
				Toil_Sit.FindSpot(parent, pawn, out spot, forced);
			}
			else
			{   
				spot = chair.Position;
			}

			if (chair != null) next_spot = new LocalTargetInfo(chair);
			else if (spot != IntVec3.Invalid) next_spot = new LocalTargetInfo(spot);
			else next_spot = null;
		}

		public override IEnumerable<Gizmo> CompGetGizmosExtra()
		{
			Command_Toggle study_toggle = new Command_Toggle();
			study_toggle.icon = ContentFinder<Texture2D>.Get("open_book", true);
			study_toggle.defaultLabel = "Study Desk";
			study_toggle.defaultDesc = "Designate this table as a study desk, where colonists can study or mentor others to develop their tech background.";
			study_toggle.isActive = () => active;
			study_toggle.toggleAction = delegate
			{
				CompGatherSpot c = parent.GetComp<CompGatherSpot>();
				this.Active = !active;
				if (active) c.Active = false;
			};

			yield return study_toggle;
		}

		override public void PostExposeData()
		{
			base.PostExposeData();
			Scribe_Values.Look<bool>(ref this.active, "study_active", false, false);
			Scribe_Collections.Look<Pawn>(ref this.students, "students", LookMode.Reference, new object[0]);
			Scribe_References.Look<Pawn>(ref this.mentor, "mentor", false);
		}
	}
}


