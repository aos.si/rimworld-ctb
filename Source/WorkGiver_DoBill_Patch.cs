using System;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace TechBackground
{
    [HarmonyPatch(typeof(WorkGiver_DoBill), "StartOrResumeBillJob")]
	public class DoBill_StartOrResumeBillJob
	{
	    [HarmonyPrefix]
		static bool Prefix(ref List<Bill> __state, WorkGiver_DoBill __instance, ref Job __result, Pawn pawn, IBillGiver giver)
		{
			TraitDef def = TraitDef.Named("TechBackground");
			if (def == null)
			{
				Log.Message("Couldn't find Trait TechBackground");
				return true;
			}
			int pawn_tech_level = 0;

			if (pawn.story.traits.HasTrait(def))
			{
				Trait tr = pawn.story.traits.GetTrait(def);
				pawn_tech_level = tr.Degree;
			}

			BillStack s = giver.BillStack;
			__state = new List<Bill>(s.Bills);

			int n_were = 0;
			int n_left = 0;

			for (int i = 0; i < s.Count; i++)
			{

				TechLevel techLevel = TechLevel.Animal;
				RecipeDef r = s[i].recipe;

				if (s[i].ShouldDoNow() && ((r.requiredGiverWorkType == null) || (r.requiredGiverWorkType == __instance.def.workType)))
				{
					// if the bill is suspended or paused do not count it as a removed bill

					n_were++;
					n_left++;

					ResearchProjectDef prereq = r.researchPrerequisite;

					if (prereq != null)
					{
						if (prereq.techLevel > techLevel) techLevel = prereq.techLevel;
					}

					if (!TechLevelMatcher.Match(pawn_tech_level, techLevel))
					{
						s.Bills.Remove(s[i]);
						// removing a bill shifts the rest of the bills by one
						i--;
						n_left--;
					}
				}
			}
			if ((n_were > 0) && (n_left == 0))
			{
				JobFailReason.Is("Colonist's tech background is too low for any bills.");
			}

			return true;
		}

	    [HarmonyPostfix]
		static void Postfix(List<Bill> __state, IBillGiver giver)
		{
			BillStack s = giver.BillStack;

			s.Clear();

			for (int i = 0; i < __state.Count; i++)
			{
				s.AddBill(__state[i]);
			}
		}
	}
}
