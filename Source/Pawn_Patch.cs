using System;
using HarmonyLib;
using RimWorld;
using Verse;

namespace TechBackground
{

    [HarmonyPatch(typeof(Pawn), "ExposeData")]
	public class Pawn_ExposeData
	{
	    [HarmonyPostfix]
		static void Postfix(Pawn __instance)
		{
		    TraitDef def = TraitDef.Named("TechBackground");
		    if (__instance.story == null) return;
		    if (!__instance.story.traits.HasTrait(def)) return;

		    if (Scribe.mode == LoadSaveMode.LoadingVars)
		    {
			Comp_TechBackground_Data comp = new Comp_TechBackground_Data();
			comp.parent = __instance;
			comp.PostExposeData();
			__instance.AllComps.Add(comp);
		    }
		}
	}
}
