using System;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace TechBackground
{
	public class WorkGiver_Study : WorkGiver_Scanner
	{

		public override ThingRequest PotentialWorkThingRequest
		{
			get
			{
				return ThingRequest.ForGroup(ThingRequestGroup.BuildingArtificial);
			}
		}

		public override bool HasJobOnThing(Pawn pawn, Thing t, bool forced = false)
		{
			TraitDef def = TraitDef.Named("TechBackground");
			Trait trait = pawn.story.traits.GetTrait(def);

			Building target = t as Building;
			if (target == null) return false;
			Comp_StudyDesk c = target.GetComp<Comp_StudyDesk>();
			if (c == null) return false;
			if (!c.Active) return false;

			if (trait.Degree >= 2)
			{
				JobFailReason.Is("Already has " + trait.CurrentData.label + " background.");
				return false;
			}

			Comp_TechBackground_Data d = pawn.GetComp<Comp_TechBackground_Data>();

			if (d.progress_today > Comp_TechBackground_Data.MaxProgressPerDay)
			{
				JobFailReason.Is("Learned max today.");
				return false;
			}

			bool can_study = false;

			foreach (Pawn p in pawn.Map.mapPawns.FreeColonists)
			{
				Trait tr = p.story.traits.GetTrait(def);
				if (tr.Degree > trait.Degree)
				{
					can_study = true;
					break;
				}
			}

			if (!can_study)
			{
				JobFailReason.Is("There are no colonists with more advanced technological background.");
				return false;
			}

			LocalTargetInfo info = c.NextValidSpot(pawn, forced);

			if (info == null)
			{
				JobFailReason.Is("No available spots around this desk.");
				return false;
			}

			return pawn.CanReserve(target, c.MaxPawns, 0, null, forced);
		}

		public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false)
		{
			Building target = t as Building;
			if (target == null) return null;
			Comp_StudyDesk c = target.GetComp<Comp_StudyDesk>();
			if (c == null) return null;

			LocalTargetInfo info = c.NextValidSpot(pawn, forced);
			return new Job(DefDatabase<JobDef>.GetNamed("TechBackground_Job_Study"), t, info);
		}
	}
}


