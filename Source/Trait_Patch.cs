using System;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;
using System.Text;

namespace TechBackground
{
    [HarmonyPatch(typeof(Trait), "TipString")]
	public class Trait_TipString
	{
	    [HarmonyPostfix]
		static void Postfix(Trait __instance, ref string __result, Pawn pawn)
		{
			if (__instance.def.defName != "TechBackground") return;

			Comp_TechBackground_Data c = pawn.GetComp<Comp_TechBackground_Data>();

			if (c == null) return;

			if (__instance.Degree < 2)
			{
				StringBuilder s = new StringBuilder(__result);
				s.AppendLine();
				s.AppendLine();
				s.AppendLine("Progress: " + c.progress.ToString("F2") + "/" + Comp_TechBackground_Data.RequiredProgress[__instance.Degree]);
				s.AppendLine("Progress today: " + c.progress_today.ToString("F2") + "/" + Comp_TechBackground_Data.MaxProgressPerDay);
				s.Append("Has Mentor: " + ((c.mentoring > 0) ? "Yes" : "No"));
				//s.Append("Has Mentor: " + c.mentoring);

				__result = s.ToString();
			}
		}
	}
}
