using System;
using System.Collections.Generic;
using Verse;

namespace TechBackground
{
    public class RoomRoleWorker_StudyRoom : RoomRoleWorker
    {
	public override float GetScore(Room room)
	{
	    int num = 0;
	    List<Thing> containedAndAdjacentThings = room.ContainedAndAdjacentThings;
	    for (int i = 0; i < containedAndAdjacentThings.Count; i++)
	    {
		Building thing = (containedAndAdjacentThings[i] as Building);
		if (thing == null) continue;
		
		Comp_StudyDesk c = thing.GetComp<Comp_StudyDesk>();
		if ((c != null) && c.Active) num++;
	    }
	    return (float)num * 16f;
	}
    }
}

