using System;
using HarmonyLib;
using RimWorld;
using Verse;
using System.Reflection;


namespace TechBackground
{
	[StaticConstructorOnStartup]
	static class HarmonyPatches
	{
		static HarmonyPatches()
		{
			Harmony harmony = new Harmony("rimworld.TechBackground");
			harmony.PatchAll(Assembly.GetExecutingAssembly());
		}
	}

}

