using System;
using HarmonyLib;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace TechBackground
{
    [HarmonyPatch(typeof(CompGatherSpot))]
    [HarmonyPatch("Active", MethodType.Setter)]
	public class CompGatherSpot_Active
	{
	    [HarmonyPostfix]
		static void Postfix(CompGatherSpot __instance)
		{
		    Comp_StudyDesk c = __instance.parent.GetComp<Comp_StudyDesk>();
		    if (c == null) return;
		    if (__instance.Active) c.Active = false;
		}
	}
}
