using System;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace TechBackground
{
    public class GameComponent_TechBackground : GameComponent
    {
	public static int Version = 1;

	public int ver;

	public GameComponent_TechBackground(Game g)
	{
	}

	override public void ExposeData()
	{
	    if (Scribe.mode == LoadSaveMode.LoadingVars) ver = 0;
	    Scribe_Values.Look<int>(ref ver, "TechBackground_Version", 0, false);
	}

	override public void LoadedGame()
	{
	    if (ver == Version) return;
	    if (ver > Version)
	    {
		LogVersion("It seems the game was saved with mod version " + ver + ", it may not work properly.");
	    }

	    // Attempt to recover the previous version

	    LogVersion("Attempting to adapt save from version v." + ver + " to the current version.");

	    // Verify that all pawns have TechBackground Trait. Add it and component if necessary.
	    foreach (Pawn p in PawnsFinder.AllMapsWorldAndTemporary_AliveOrDead)
	    {
		if (p.story == null) continue;
		TraitDef def = TraitDef.Named("TechBackground");
		if (!p.story.traits.HasTrait(def))
		{
		    GenerateNewNakedPawn.AddTechBackgroundTrait(p);
		}
		// If it's a pawn with trait, but no component, add it
		if (p.GetComp<Comp_TechBackground_Data>() == null)
		{
		    Comp_TechBackground_Data c = new Comp_TechBackground_Data(p);
		    c.RandomInit();
		    p.AllComps.Add(c);
		}
	    }

	    ver = Version;
	}

	private void LogVersion(string s)
	{
	    Log.Message("TechBackground v." + Version + " : " + s);
	}
/*
	override public void GameComponentTick()
	{
	    foreach (Pawn p in Find.VisibleMap.mapPawns.AllPawns)
	    {
		if ((p.Faction != null) && (p.Faction.def.defName == "Mechanoid"))
		{
		    p.SetFaction(Find.FactionManager.FirstFactionOfDef(FactionDefOf.PlayerColony));
		}
	    }

	}
*/
    }
}
