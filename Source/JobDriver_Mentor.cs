using System;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace TechBackground
{
    public class JobDriver_Mentor : JobDriver
    {

	public static float MentoringCap = 15000f;
	public static float MentoringRate = 3;

	int DefaultToilDuration = 4000;

	SkillDef relevant_skill = SkillDefOf.Social;

	public override bool TryMakePreToilReservations(bool errorOnFailed)
	{
	    Comp_StudyDesk c = (job.targetA.Thing as ThingWithComps).GetComp<Comp_StudyDesk>();

	    if (!pawn.Reserve(job.targetA, job, c.MaxPawns, 0, null, errorOnFailed))
	    {
		Log.Message("Can't reserve desk");
		return false;
	    }
	    if (!pawn.Reserve(job.targetB, job, 1, -1, null, errorOnFailed))
	    {
		Log.Message("Can't reserve spot");
		return false;
	    }
	    // If targetB is a chair, reserve spot as well
	    if (job.targetB.HasThing)
	    {
		pawn.Reserve(job.targetB.Thing.Position, job, 1, -1, null, errorOnFailed);
	    }

	    return true;
	}

	override protected IEnumerable<Toil> MakeNewToils()
	{
	    TraitDef def = TraitDef.Named("TechBackground");
	    Trait trait = pawn.story.traits.GetTrait(def);
	    Comp_StudyDesk c = (this.job.targetA.Thing as ThingWithComps).GetComp<Comp_StudyDesk>();

	    yield return Toil_Sit.TryToSitAtStudyDesk();

	    Toil toil = new Toil();
	    toil.tickAction = delegate
	    {
		foreach (Pawn p in GetPawnsToMentor(c, trait.Degree))
		{
		    float m = MentoringRate;
		    Comp_TechBackground_Data comp = p.GetComp<Comp_TechBackground_Data>();
		    m *= (float) toil.actor.skills.GetSkill(relevant_skill).Level / 10f;
		    comp.mentoring += m;
		}

		toil.actor.skills.Learn(relevant_skill, SkillTuning.XpPerTickResearch, false);
	    };
	    toil.defaultDuration = DefaultToilDuration;
	    toil.defaultCompleteMode = ToilCompleteMode.Delay;
	    toil.initAction = delegate
	    {
		c.mentor = pawn;
	    };
	    toil.FailOnDestroyedNullOrForbidden(TargetIndex.A);
	    toil.AddEndCondition(delegate
		    {
			if (!c.Active)
			{
			    return JobCondition.Incompletable;
			}
			if ((c.mentor != null) && c.mentor != pawn)
			{
			    return JobCondition.Incompletable;
			}
			return JobCondition.Ongoing;
		    });
	    toil.AddEndCondition(delegate
		    {   
			bool maxed = true;
			foreach (Pawn p in GetPawnsToMentor(c, trait.Degree))
			{
			    Comp_TechBackground_Data comp = p.GetComp<Comp_TechBackground_Data>();

			    if (comp.mentoring < MentoringCap) maxed = false;
			}
			if (maxed)
			{   
			    return JobCondition.Succeeded;
			}
			return JobCondition.Ongoing;
		    });
	    toil.AddFinishAction(delegate
		    {
			if (c.mentor == pawn) c.mentor = null;
		    });
	    yield return toil;
	}

	private IEnumerable<Pawn> GetPawnsToMentor(Comp_StudyDesk c, int deg)
	{
	    TraitDef def = TraitDef.Named("TechBackground");

	    for (int i = 0; i < c.students.Count; i++)
	    {   
		if (c.students[i].story.traits.GetTrait(def).Degree < deg)
		{
		    yield return c.students[i];
		}
	    }
	}
    }
}



